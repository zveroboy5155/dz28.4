#include "main_header.h"
#include <mutex>
#include <thread>

#define FOR_CYCLE(size, code) for(int i = 0 ; i < size; i++){code}
#define SWIMMER_NUM 6


std::mutex start_acess;
static bool startFl;
std::vector<int> rate;

void swimming(int speed, int num){
    int dist = 0, cur_time = 0;

    start_acess.lock();
    while(!startFl);
    std::cout << "������ " << num <<" ����� ������" << std::endl;
    start_acess.unlock();
    while(dist < 100){
        std::this_thread::sleep_for(std::chrono::seconds(1));
        cur_time++;
        dist = cur_time*speed;
        if(dist >= 100) break;
        start_acess.lock();
        std::cout << "������ " << num << " ������� " << dist << std::endl;
        start_acess.unlock();
    }
    start_acess.lock();
    rate.push_back(num);
    std::cout << "������ " << num << " �������� ������ " << std::endl;
    start_acess.unlock();
}


void task1(){
    int step;
    int speed[SWIMMER_NUM];
    std::thread *calls[SWIMMER_NUM];

    //std::cout << "������ ������� 1" <<std::endl;

    for(int i = 0 ; i < SWIMMER_NUM; i++){
        std::cout << "������� �������� " << i + 1 << " ������" <<std::endl;
        std::cin >> speed[i];
    }
    //����������, ���� ��� ������ �� ������ �� �����
    start_acess.lock();
    startFl = false;
    FOR_CYCLE(SWIMMER_NUM, calls[i] = new std::thread(swimming, speed[i], i + 1);)
    /*for(int i = 1 ; i <= 6; i++){
        calls[i] = new std::thread(swimming, speed[i]);
        //call.detach();
    }*/
    startFl = true;
    start_acess.unlock();
    FOR_CYCLE(SWIMMER_NUM, calls[i]->join();)

    FOR_CYCLE(SWIMMER_NUM, delete(calls[i]);)
    /*for(int i = 1 ; i <= 6; i++)
        calls[i]->join();*/
    FOR_CYCLE(SWIMMER_NUM, std::cout << i + 1 << " ����� - ������ �" << rate[i] << std::endl;)
}

