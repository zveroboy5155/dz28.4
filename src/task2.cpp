#include "main_header.h"
#include <mutex>
#include <thread>

#define FOR_CYCLE(size, code) for(int i = 0 ; i < size; i++){code}
#define TRAIN_NUM 3


std::mutex station_acess;
static bool startFl, stationFl;


void trainDirect(int time, int trNum){
    std::string cmd;

    station_acess.lock();
    while (!startFl);
    station_acess.unlock();
    std::this_thread::sleep_for(std::chrono::seconds(time));
    station_acess.lock();
    stationFl = true;
    std::cout << "����� " << char ('A' + trNum) << " ������ �� ������, ������� \"depart\" ��� �����������" << std::endl;
    while(cmd != "depart") std::cin >> cmd;
    stationFl = false;
    std::cout << "����� " << char  ('A' + trNum) << " ����������" << std::endl;
    station_acess.unlock();
}


void task2(){
    int wayTime;
    std::thread *trains[TRAIN_NUM];
    std::cout << "������ ������� 2" <<std::endl;
    station_acess.lock();
    startFl = false;
    stationFl = true;
    for(int i = 0; i < 3; i++){
        std::cout << "������� ����� � ���� " << i + 1 << " ������" <<std::endl;
        std::cin >> wayTime;
        trains[i] = new std::thread(trainDirect, wayTime, i);
    }
    startFl = true;
    station_acess.unlock();
    FOR_CYCLE(TRAIN_NUM, trains[i]->join();)

    FOR_CYCLE(TRAIN_NUM, delete(trains[i]);)


}