#include "main_header.h"
#include <mutex>
#include <thread>

#include <stack>
std::mutex kitchen_acess, deliv_acess;
static bool stopAllFl, stationFl;
std::stack<int> orderList;
std::stack<int> deliverList;


void deliver(){
    int sucDel = 0;
    std::string dishes[5] = {"�����", "���", "�����", "�����", "����"};

    while(sucDel < 10){
        if (deliverList.empty()) {
            std::this_thread::sleep_for(std::chrono::seconds(5));
            continue;
        }
        deliv_acess.lock();
        std::cout << "������ ��������:\n";
        while(!deliverList.empty()) {
            std::cout << dishes[deliverList.top()] << "\n";
            deliverList.pop();
            sucDel++;
        }
        std::cout << std::endl;
        deliv_acess.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(30));
    }
    kitchen_acess.lock();
    stopAllFl = true;
    kitchen_acess.unlock();
}


void cook(){
    int dishNum;
    std::string dishes[5] = {"�����", "���", "�����", "�����", "����"};

    while(!stopAllFl) {

        if (orderList.empty()) {
            std::this_thread::sleep_for(std::chrono::seconds(5));
            continue;
        }
        kitchen_acess.lock();
        dishNum = orderList.top();
        std::cout << "����� " << dishes[dishNum] << " ������ ����������" << std::endl;
        orderList.pop();
        kitchen_acess.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(rand() % 11 + 5));
        kitchen_acess.lock();
        std::cout << "����� " << dishes[dishNum] << " ������������ � ������ � ��������" << std::endl;
        kitchen_acess.unlock();
        deliv_acess.lock();
        deliverList.push(dishNum);
        deliv_acess.unlock();

        //if (stopAllFl) break;
    }
    kitchen_acess.lock();
    std::cout << "����� �������" << std::endl;
    kitchen_acess.unlock();
}

void task3(){
    int dishNum;
    std::string dishes[5] = {"�����", "���", "�����", "�����", "����"};

    //for(auto it :dishes) std::cout << dishes <<std::endl;

    std::cout << "������ ������� 3" << std::endl;

    stopAllFl = false;
    std::thread courier(deliver);
    courier.detach();
    std::thread kitchen(cook);
    kitchen.detach();
    while(!stopAllFl){
        dishNum = rand() % 5 + 0;
        kitchen_acess.lock();
        //std::cout << "����� dishNum =" << dishNum << std::endl;
        std::cout << "����� �� " << (std::string) dishes[dishNum] << " ������� ����������" << std::endl;
        orderList.push(dishNum);
        kitchen_acess.unlock();
        std::this_thread::sleep_for(std::chrono::seconds((rand() % 6 + 5)));
    }
    //kitchen.join();
    //courier.join();
    std::cout << "10 ������� ����������" << std::endl;
}